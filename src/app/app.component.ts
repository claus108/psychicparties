import { Component, ViewEncapsulation, HostListener} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  navbarFixed = false;

  constructor() {}

   @HostListener("window:scroll", [])
   onWindowScroll() {
     let number = window.scrollY;
     if (number > 200) {
       this.navbarFixed = true;
      } else if ( this.navbarFixed == true && number  == 0) {
        this.navbarFixed = false;
      }
    }
}
