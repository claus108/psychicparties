import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

import { AppComponent } from '../app.component';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnChanges {

  public isCollapsed = true;

  lightTheme = true;
  darkTheme = false;
  home = true;
  title = 'Fun Pyshic Parties';

  @Input() fixNavbar = false;

  constructor(
    private app: AppComponent) { }

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {

    this.fixNavbar = changes['fixNavbar'].currentValue;
    if (this.home) {
      this.fixNavbar ? this.lightTheme = false : this.lightTheme = true;
      console.log('light theme? ', this.lightTheme);
    }
  }

  setStatus(status: any) {
    if (status === 'home') {
      this.fixNavbar ? this.lightTheme = false : this.lightTheme = true;
      this.home = true;
    } else {
      this.lightTheme = false;
      this.home = false;
    }
  }
}
